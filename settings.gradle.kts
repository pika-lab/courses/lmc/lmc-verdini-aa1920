rootProject.name = "2p"

include("core")
include("unify")
include("theory")
include("dsl-core")
include("dsl-unify")
include("dsl-theory")
include("solve")
include("solve-classic")
include("solve-streams")
include("solve-test")
